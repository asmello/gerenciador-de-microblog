#include "users.h"

quint64 Users::num_nodes = 0;

void Users::alphabeticalSort(Node * n, QList<QString> &list)
{
    if (n == NULL) return;
    alphabeticalSort(n->left, list);
    list.append(n->key);
    alphabeticalSort(n->right, list);
}

QList<QString> Users::getSortedListAlphabetical()
{
    QList<QString> sortedList;
    alphabeticalSort(root, sortedList);
    return sortedList;
}

void Users::mapCreate(Node * n, QMap<quint64, QString> &map)
{
    if (n == NULL) return;
    mapCreate(n->left, map);
    map.insert(n->data, n->key);
    mapCreate(n->right, map);
}

QList<QString> Users::getSortedListChronological()
{
    QMap<quint64, QString> sortedMap;
    mapCreate(root, sortedMap);
    return sortedMap.values();
}
