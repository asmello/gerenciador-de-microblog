#ifndef USERMESSAGES_H
#define USERMESSAGES_H

#include "userlist.h"
#include "messages.h"
#include "extension.h"
#include <QAbstractTableModel>
#include <QFile>
#include <QDataStream>

class UserMessages : public QAbstractTableModel
{
    Q_OBJECT
    friend class MessageList;
private:
    QList<Messages::msglistdata> usermsgs;
    Messages * msgstree;
    Users * userstree;

public:
    explicit UserMessages(UserList * userlist, QObject * parent = 0);
    ~UserMessages();

    bool add(QString user, QString msg);
    bool remove(QString user, int index);
    bool isActive(QString user) { return msgstree->test(user); }

    bool clear(QString user);

    int getNumMsgs(QString user) { return msgstree->getNumMsgs(user); }
    int getTotalMsgs();
    int getNumActive();
    QList<QString> getTopKeys(int amount);
    QList<QString> getTopUser();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    void writeToFile(QFile &file);
    void readFromFile(QFile &file);

signals:
    void needUpdate();

public slots:
    void update();
    void filter(QString);
    void filterOut(QString);
    void filterUsers(QString);
    void filterOutUsers(QString);
    void cellAction(QModelIndex);
};

#endif // USERMESSAGES_H
