#include "remmsgdialog.h"
#include "ui_remmsgdialog.h"

#include <QDebug>

RemMsgDialog::RemMsgDialog(UserMessages * usermsgs, QWidget * parent) :
    QDialog(parent),
    ui(new Ui::RemMsgDialog)
{
    ui->setupUi(this);
    this->usermsgs = usermsgs;
}

RemMsgDialog::~RemMsgDialog()
{
    delete ui;
}

QString RemMsgDialog::getUser() const
{
    return ui->lineEdit->text();
}

int RemMsgDialog::getIndex() const
{
    return ui->spinBox->value()-1;
}

void RemMsgDialog::updateSpinBox(QString text)
{
    if (!usermsgs->isActive(text)) {
        if (ui->spinBox->isEnabled()) {
            ui->spinBox->setRange(0, 0);
            ui->spinBox->setEnabled(false);
        }
        return;
    }
    ui->spinBox->setRange(1, usermsgs->getNumMsgs(text));
    if (!ui->spinBox->isEnabled()) ui->spinBox->setEnabled(true);
}
