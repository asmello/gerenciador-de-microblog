#ifndef MESSAGEEXPANDWINDOW_H
#define MESSAGEEXPANDWINDOW_H

#include <QDialog>

namespace Ui {
class MessageExpandWindow;
}

class MessageExpandWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MessageExpandWindow(QWidget *parent = 0);
    ~MessageExpandWindow();

    void setMessage(QString message);
    void setUserInfo(QString user, int id);

private:
    Ui::MessageExpandWindow *ui;
};

#endif // MESSAGEEXPANDWINDOW_H
