#ifndef MESSAGELIST_H
#define MESSAGELIST_H

#include "usermessages.h"
#include <QAbstractListModel>

class MessageList : public QAbstractListModel
{
    Q_OBJECT
private:
    QString user;
    QList<QString> messages;
    Messages * msgtree;
public:
    explicit MessageList(UserMessages * usermsgs, QObject *parent = 0);

    QString getUser() const { return user; }

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

signals:
    void needUpdate(QString);
    void needUpdate();

public slots:
    void update();
    void update(QString);
    void filter(QString);
    void filterOut(QString);
    void setUser(QString);
    void cellAction(QModelIndex);
};

#endif // MESSAGELIST_H
