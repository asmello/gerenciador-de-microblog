#include "userlist.h"

UserList::UserList(QObject *parent) :
    QAbstractListModel(parent), useChronological(false)
{
    usertree = new Users();
    connect(this, SIGNAL(needUpdate()), this, SLOT(update()));
}

UserList::~UserList()
{
    delete usertree;
}

int UserList::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    return usernames.size();
}

QVariant UserList::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole) return QVariant();
    return QVariant(usernames.at(index.row()));
}
QVariant UserList::headerData(int section,
                              Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) return QVariant();
    if (orientation == Qt::Horizontal) {
        if (section == 0) return QVariant(trUtf8("Usuário"));
        else return QVariant(section);
    } else return QVariant(section+1);
}

// improve
void UserList::update()
{
    emit beginResetModel();
    if (useChronological) usernames = usertree->getSortedListChronological();
    else usernames = usertree->getSortedListAlphabetical();
    emit endResetModel();
}

bool UserList::add(QString username)
{
    if (usertree->isRegistered(username)) return false;
    usertree->add(username);
    emit needUpdate();
    return true;
}

bool UserList::remove(QString username)
{
    if (!usertree->isRegistered(username)) return false;
    usertree->remove(username);
    emit needUpdate();
    return true;
}

void UserList::setChronological(bool value)
{
    useChronological = value;
    emit needUpdate();
}

bool UserList::isRegistered(QString username)
{
    return usertree->isRegistered(username);
}

void UserList::filter(QString text)
{
    emit beginResetModel();
    for (int i = 0; i < usernames.size(); ++i) {
        if (!usernames.at(i).contains(text)) usernames.removeAt(i--);
    }
    emit endResetModel();
}

void UserList::filterOut(QString text)
{
    emit beginResetModel();
    for (int i = 0; i < usernames.size(); ++i) {
        if (usernames.at(i).contains(text)) usernames.removeAt(i--);
    }
    emit endResetModel();
}

void UserList::writeToFile(QFile &file)
{
    QDataStream out(&file);
    out << *usertree;
}

void UserList::readFromFile(QFile &file)
{
    QDataStream in(&file);
    in >> *usertree;
    emit needUpdate();
}

void UserList::cellAction(QModelIndex index)
{

}
