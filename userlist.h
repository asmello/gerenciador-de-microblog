#ifndef USERLIST_H
#define USERLIST_H

#include "users.h"
#include <QAbstractListModel>
#include <QFile>
#include <QDataStream>

class UserList : public QAbstractListModel
{
    Q_OBJECT
    friend class UserMessages;
private:
    Users * usertree;
    QList<QString> usernames;
    bool useChronological;

public:
    explicit UserList(QObject * parent = 0);
    ~UserList();

    bool add(QString username);
    bool remove(QString username);
    bool isRegistered(QString username);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    void writeToFile(QFile &file);
    void readFromFile(QFile &file);

signals:
    void needUpdate();

public slots:
    void update();
    void filter(QString);
    void filterOut(QString);
    void setChronological(bool);
    void cellAction(QModelIndex);
};

#endif // USERLIST_H
