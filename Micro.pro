#-------------------------------------------------
#
# Project created by QtCreator 2013-12-31T15:52:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MicroGerenciador
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    users.cpp \
    userlist.cpp \
    usermessages.cpp \
    messages.cpp \
    newmsgdialog.cpp \
    remmsgdialog.cpp \
    messagelist.cpp \
    statisticswindow.cpp \
    messageexpandwindow.cpp

HEADERS  += mainwindow.h \
    avl.h \
    users.h \
    userlist.h \
    usermessages.h \
    messages.h \
    newmsgdialog.h \
    remmsgdialog.h \
    messagelist.h \
    statisticswindow.h \
    extension.h \
    messageexpandwindow.h

FORMS    += mainwindow.ui \
    newmsgdialog.ui \
    remmsgdialog.ui \
    statisticswindow.ui \
    messageexpandwindow.ui
