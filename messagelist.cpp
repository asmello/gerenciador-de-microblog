#include "messagelist.h"
#include "messageexpandwindow.h"
#include <QDebug>

MessageList::MessageList(UserMessages *usermsgs, QObject *parent) :
    QAbstractListModel(parent)
{
    msgtree = usermsgs->msgstree;
    connect(this, SIGNAL(needUpdate()), this, SLOT(update()));
}

int MessageList::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    return messages.size();
}

QVariant MessageList::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole) return QVariant();
    return QVariant(messages.at(index.row()));
}

void MessageList::setUser(QString user)
{
    this->user = user;
    emit needUpdate();
}

QVariant MessageList::headerData(int section,
                                 Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) return QVariant();
    if (orientation == Qt::Horizontal) {
        if (section == 0) return QVariant(trUtf8("Mensagem"));
        else return QVariant(section);
    } else return QVariant(section+1);
}

void MessageList::update(QString user)
{
    if (this->user != user) return;
    emit beginResetModel();
    messages = msgtree->getMessages(user);
    emit endResetModel();
}

void MessageList::update()
{
    emit beginResetModel();
    messages = msgtree->getMessages(user);
    emit endResetModel();
}

void MessageList::filter(QString text)
{
    emit beginResetModel();
    for (int i = 0; i < messages.size(); ++i) {
        if (!messages.at(i).contains(text)) messages.removeAt(i--);
    }
    emit endResetModel();
}

void MessageList::filterOut(QString text)
{
    emit beginResetModel();
    for (int i = 0; i < messages.size(); ++i) {
        if (messages.at(i).contains(text)) messages.removeAt(i--);
    }
    emit endResetModel();
}

void MessageList::cellAction(QModelIndex index)
{
    MessageExpandWindow msgdetails;
    msgdetails.setMessage(messages.at(index.row()));
    msgdetails.setUserInfo(user, index.row()+1);
    msgdetails.exec();
}
