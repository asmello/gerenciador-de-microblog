#ifndef EXTENSION_H
#define EXTENSION_H

#include <QList>
#include <algorithm>

template <typename T>
QList<T> reversed(const QList<T> &in) {
    QList<T> result;
    result.reserve(in.size()); // reserve is new in Qt 4.7
    std::reverse_copy(in.begin(), in.end(), std::back_inserter(result));
    return result;
}

#endif // EXTENSION_H
