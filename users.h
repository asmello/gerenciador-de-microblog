#ifndef USERS_H
#define USERS_H

#include "avl.h"
#include <QMap>
#include <QList>
#include <QString>

class Users : public AVL<QString, quint64>
{
private:
    static quint64 num_nodes;
    static void alphabeticalSort(Node * n, QList<QString> &list);
    static void mapCreate(Node * n, QMap<quint64, QString> &map);
    void serialize(QDataStream &stream) const {
        stream << num_nodes;
        recursiveSerialize(stream, root);
    }
    void deserialize(QDataStream &stream) {
        stream >> num_nodes;
        recursiveDeserialize(stream, root);
    }

public:
    void add(QString username)
    { AVL<QString, quint64>::add(username, num_nodes++); }
    void remove(QString username)
    { AVL<QString, quint64>::remove(username); }
    bool isRegistered(QString username)
    { return AVL<QString, quint64>::test(username); }
    QList<QString> getSortedListAlphabetical();
    QList<QString> getSortedListChronological();
};

#endif // USERS_H
