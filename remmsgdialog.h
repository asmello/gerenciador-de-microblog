#ifndef REMMSGDIALOG_H
#define REMMSGDIALOG_H

#include "userlist.h"
#include "usermessages.h"
#include <QDialog>

namespace Ui {
class RemMsgDialog;
}

class RemMsgDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RemMsgDialog(UserMessages * usermsgs, QWidget *parent = 0);
    ~RemMsgDialog();

    QString getUser() const;
    int getIndex() const;

private:
    Ui::RemMsgDialog *ui;
    UserMessages * usermsgs;

private slots:
    void updateSpinBox(QString);
};

#endif // REMMSGDIALOG_H
