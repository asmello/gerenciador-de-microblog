#include "statisticswindow.h"
#include "ui_statisticswindow.h"
#include <QPalette>

StatisticsWindow::StatisticsWindow(UserMessages *usermsgs, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StatisticsWindow)
{
    ui->setupUi(this);
    ui->lcdNumber->display(usermsgs->getNumActive());
    ui->lcdNumber_2->display(usermsgs->getTotalMsgs());
    QString topkeys;
    foreach(QString str, usermsgs->getTopKeys(3)) {
        topkeys.append(str + ", ");
    }
    if (!topkeys.isEmpty()) topkeys.chop(2);
    ui->label_5->setText(topkeys);
    QString topuser;
    foreach(QString str, usermsgs->getTopUser()) {
        topuser.append(str + ", ");
    }
    if (!topuser.isEmpty()) topuser.chop(2);
    ui->label_8->setText(topuser);
}

StatisticsWindow::~StatisticsWindow()
{
    delete ui;
}


