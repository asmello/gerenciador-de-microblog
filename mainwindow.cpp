#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newmsgdialog.h"
#include "remmsgdialog.h"
#include "statisticswindow.h"

#include <QInputDialog>
#include <QMessageBox>
#include <QFileDialog>

void MainWindow::reset()
{
    clear();
    set();
}

void MainWindow::clear()
{
    delete msglist;
    delete usermsgs;
    delete userlist;
}

void MainWindow::set()
{
    connect(this, SIGNAL(modelChanged(Model)),
            this, SLOT(updateModel(Model)));
    emit modelChanged(INVALID);

    userlist = new UserList(this);
    connect(this, SIGNAL(filterAddedUsers(QString)),
            userlist, SLOT(filter(QString)));
    connect(this, SIGNAL(filterOutAddedUsers(QString)),
            userlist, SLOT(filterOut(QString)));
    connect(this, SIGNAL(filterResetUsers()),
            userlist, SLOT(update()));
    connect(this, SIGNAL(chronologicalSortSet(bool)),
            userlist, SLOT(setChronological(bool)));

    usermsgs = new UserMessages(userlist, this);
    connect(this, SIGNAL(filterAdded(QString)),
            usermsgs, SLOT(filter(QString)));
    connect(this, SIGNAL(filterOutAdded(QString)),
            usermsgs, SLOT(filterOut(QString)));
    connect(this, SIGNAL(filterAddedUsers(QString)),
            usermsgs, SLOT(filterUsers(QString)));
    connect(this, SIGNAL(filterOutAddedUsers(QString)),
            usermsgs, SLOT(filterOutUsers(QString)));
    connect(this, SIGNAL(filterReset()),
            usermsgs, SLOT(update()));
    connect(this, SIGNAL(filterResetUsers()),
            usermsgs, SLOT(update()));

    msglist = new MessageList(usermsgs, this);
    connect(this, SIGNAL(filterAdded(QString)),
            msglist, SLOT(filter(QString)));
    connect(this, SIGNAL(filterOutAdded(QString)),
            msglist, SLOT(filterOut(QString)));
    connect(this, SIGNAL(filterReset()),
            msglist, SLOT(update()));
    connect(this, SIGNAL(msgsChange(QString)),
            msglist, SLOT(update(QString)));
    connect(this, SIGNAL(msgListUserSet(QString)),
            msglist, SLOT(setUser(QString)));

    emit modelChanged(USERLIST);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    set();
}

MainWindow::~MainWindow()
{
    clear();
    delete ui;
}

void MainWindow::updateModel(Model model)
{
    if (model == currModel) return;
    disconnect(ui->tableView, SIGNAL(doubleClicked(QModelIndex)), 0, 0);
    switch (model) {
    case USERLIST:
        ui->tableView->setModel(userlist);
        connect(ui->tableView, SIGNAL(doubleClicked(QModelIndex)),
                userlist, SLOT(cellAction(QModelIndex)));
        ui->label->setText(trUtf8("Usuários Registrados"));
        break;
    case USERMSGS:
        ui->tableView->setModel(usermsgs);
        connect(ui->tableView, SIGNAL(doubleClicked(QModelIndex)),
                usermsgs, SLOT(cellAction(QModelIndex)));
        ui->label->setText(trUtf8("Usuários ativos / Mensagens"));
        break;
    case MSGSLIST:
        ui->tableView->setModel(msglist);
        connect(ui->tableView, SIGNAL(doubleClicked(QModelIndex)),
                msglist, SLOT(cellAction(QModelIndex)));
        ui->label->setText(trUtf8("Mensagens de %1").arg(msglist->getUser()));
        break;
    default:
        break;
    }
    currModel = model;
}

void MainWindow::on_actionNovo_triggered()
{
    bool ok;
    QString text = QInputDialog::getText(this, trUtf8("Cadastro de Usuário"),
                                         trUtf8("Nome do usuário:"),
                                         QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) {
        if (!userlist->add(text)) {
            QMessageBox msgBox(this);
            msgBox.setText(trUtf8("Usuário já existe."));
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        }
    }
}

void MainWindow::on_actionApagar_triggered()
{
    bool ok;
    QString text = QInputDialog::getText(this, trUtf8("Remoção de Usuário"),
                                         trUtf8("Nome do usuário:"),
                                         QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) {
        usermsgs->clear(text);
        if (!userlist->remove(text)) {
            QMessageBox msgBox(this);
            msgBox.setText(trUtf8("Usuário não encontrado."));
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        } else emit msgsChange(text);
    }
}

void MainWindow::on_actionListar_mensagens_triggered()
{
    bool ok;
    QString text = QInputDialog::getText(this, trUtf8("Listagem de Mensages"),
                                         trUtf8("Nome do usuário:"),
                                         QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) {
        if (!userlist->isRegistered(text)) {
            QMessageBox msgBox(this);
            msgBox.setText(trUtf8("Usuário não encontrado."));
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        } else {
            emit msgListUserSet(text);
            emit modelChanged(MSGSLIST);
        }
    }
}

void MainWindow::on_actionApagar_mensagens_triggered()
{
    bool ok;
    QString user = QInputDialog::getText(this, trUtf8("Limpeza de Mensagens"),
                                         trUtf8("Nome do usuário:"),
                                         QLineEdit::Normal, "", &ok);
    if (ok && !user.isEmpty()) {
        if (!usermsgs->clear(user)) {
            QMessageBox msgBox(this);
            msgBox.setText(trUtf8("Usuário não encontrado."));
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        }
    } else emit msgsChange(user);
}

void MainWindow::on_actionNova_triggered()
{
    QString user, msg;
    NewMsgDialog newmsgdialog(this);
    newmsgdialog.exec();
    if (newmsgdialog.result() == QDialog::Rejected) return;
    user = newmsgdialog.getUser();
    msg = newmsgdialog.getMsg();
    if (!usermsgs->add(user, msg)) {
        QMessageBox msgBox(this);
        msgBox.setText(trUtf8("Usuário não encontrado."));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    } else emit msgsChange(user);
}

void MainWindow::on_actionApagar_2_triggered()
{
    QString user;
    int index;
    emit filterReset(); // melhorar, usar número filtrado
    RemMsgDialog remmsgdialog(usermsgs, this);
    remmsgdialog.exec();
    if (remmsgdialog.result() == QDialog::Rejected) return;
    user = remmsgdialog.getUser();
    index = remmsgdialog.getIndex();
    if (!usermsgs->remove(user, index)) {
        QMessageBox msgBox(this);
        msgBox.setText(trUtf8("Usuário não encontrado."));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    } else emit msgsChange(user);
}

void MainWindow::on_actionLimpar_dados_triggered()
{
    QMessageBox msgBox(this);
    msgBox.setText(trUtf8("Apagar todos os dados no registro."));
    msgBox.setInformativeText(trUtf8("Tem certeza de que deseja continuar?"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Yes);
    msgBox.setIcon(QMessageBox::Question);
    if (msgBox.exec() == QMessageBox::Yes) reset();
}

void MainWindow::on_actionListar_ativos_triggered()
{
    emit modelChanged(USERMSGS);
}

void MainWindow::on_actionOrdem_Alfab_tica_triggered()
{
    ui->actionOrdem_Cronol_gica->setChecked(false);
    emit chronologicalSortSet(false);
    emit modelChanged(USERLIST);
}

void MainWindow::on_actionOrdem_Cronol_gica_triggered()
{
    ui->actionOrdem_Alfab_tica->setChecked(false);
    emit chronologicalSortSet(true);
    emit modelChanged(USERLIST);
}

void MainWindow::on_actionSobre_triggered()
{
    QMessageBox::about(this, trUtf8("Microblog"),
                       trUtf8("Versão beta.\nPor André Sá de Mello (2014)"));
}

void MainWindow::on_actionLimpar_filtros_triggered()
{
    emit filterReset();
}

void MainWindow::on_actionLimpar_filtros_2_triggered()
{
    emit filterResetUsers();
}

void MainWindow::on_actionManter_triggered()
{
    bool ok;
    QString text = QInputDialog::getText(this, trUtf8("Filtrar Usuários"),
                                         trUtf8("Texto a incluir:"),
                                         QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) emit filterAddedUsers(text);
}

void MainWindow::on_actionEliminar_triggered()
{
    bool ok;
    QString text = QInputDialog::getText(this, trUtf8("Filtrar Usuários"),
                                         trUtf8("Texto a excluir:"),
                                         QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) emit filterOutAddedUsers(text);
}

void MainWindow::on_actionSalvar_triggered()
{
    QString filename = QFileDialog::getSaveFileName(
                this, trUtf8("Salvar Arquivo de Dados"),
                QDir::homePath(), trUtf8("Arquivos de Dados (*.dat)"));
    if (filename.isEmpty()) return;
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) return;
    userlist->writeToFile(file);
    usermsgs->writeToFile(file);
    file.flush();
    file.close();
}

void MainWindow::on_actionAbrir_triggered()
{
    QString filename = QFileDialog::getOpenFileName(
                this, trUtf8("Abrir Arquivo de Dados"),
                QDir::homePath(), trUtf8("Arquivos de Dados (*.dat)"));
    if (filename.isEmpty()) return;
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) return;
    userlist->readFromFile(file);
    usermsgs->readFromFile(file);
    file.close();
}

void MainWindow::on_actionManter_2_triggered()
{
    bool ok;
    QString text = QInputDialog::getText(this, trUtf8("Filtrar Mensagens"),
                                         trUtf8("Texto a incluir:"),
                                         QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) emit filterAdded(text);
}

void MainWindow::on_actionEliminar_2_triggered()
{
    bool ok;
    QString text = QInputDialog::getText(this, trUtf8("Filtrar Mensagens"),
                                         trUtf8("Texto a excluir:"),
                                         QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) emit filterOutAdded(text);
}

void MainWindow::on_actionEstatisticas_triggered()
{
    StatisticsWindow statisticswindow(usermsgs, this);
    statisticswindow.exec();
}
