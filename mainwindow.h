#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "userlist.h"
#include "messagelist.h"
#include "usermessages.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MainWindow * ui;
    UserList * userlist;
    MessageList * msglist;
    UserMessages * usermsgs;
    enum Model {
        INVALID,
        USERLIST,
        USERMSGS,
        MSGSLIST
    } currModel;
    void set();
    void clear();
    void reset();

public:
    explicit MainWindow(QWidget * parent = 0);
    ~MainWindow();

signals:
    void msgsChange(QString);
    void filterReset();
    void filterAdded(QString);
    void filterOutAdded(QString);
    void filterResetUsers();
    void filterAddedUsers(QString);
    void filterOutAddedUsers(QString);
    void msgListUserSet(QString);
    void modelChanged(Model);
    void chronologicalSortSet(bool);

private slots:
    void updateModel(Model);
    void on_actionNovo_triggered();
    void on_actionApagar_triggered();
    void on_actionListar_mensagens_triggered();
    void on_actionApagar_mensagens_triggered();
    void on_actionNova_triggered();
    void on_actionApagar_2_triggered();
    void on_actionLimpar_dados_triggered();
    void on_actionListar_ativos_triggered();
    void on_actionOrdem_Alfab_tica_triggered();
    void on_actionOrdem_Cronol_gica_triggered();
    void on_actionSobre_triggered();
    void on_actionLimpar_filtros_triggered();
    void on_actionLimpar_filtros_2_triggered();
    void on_actionManter_triggered();
    void on_actionEliminar_triggered();
    void on_actionSalvar_triggered();
    void on_actionAbrir_triggered();
    void on_actionManter_2_triggered();
    void on_actionEliminar_2_triggered();
    void on_actionEstatisticas_triggered();
};

#endif // MAINWINDOW_H
