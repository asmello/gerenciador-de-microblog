#ifndef AVL_H
#define AVL_H

#include <cstddef>
#include <QDataStream>

template <typename K, typename V> class AVL;

template <typename K, typename V>
QDataStream &operator<<(QDataStream &stream, const AVL<K, V> &data)
{
    data.serialize(stream);
    return stream;
}

template <typename K, typename V>
QDataStream &operator>>(QDataStream &stream, AVL<K, V> &data)
{
    data.deserialize(stream);
    return stream;
}

template <typename K, typename V> class AVL
{
protected:
    struct Node {
        K key;
        V data;
        int delta;
        Node * left;
        Node * right;
        Node(K key, V data) : delta(0), left(NULL), right(NULL)
        {
            this->key = key;
            this->data = data;
        }
        ~Node()
        {
            delete left;
            delete right;
        }
    };

    Node * root;

    static void LL(Node * &n)
    {
        Node * child = n->left;
        n->left = child->right;
        child->right = n;
        n->delta = child->delta = 0;
        n = child;
    }

    static void LR(Node * &n)
    {
        Node * child = n->left;
        Node * gchild = child->right;
        n->left = gchild->right;
        child->right = gchild->left;
        gchild->left = child;
        gchild->right = n;
        switch (gchild->delta) {
        case -1:
            n->delta = 1;
            child->delta = 0;
            break;
        case 0:
            n->delta = 0;
            child->delta = 0;
            break;
        case 1:
            n->delta = 0;
            child->delta = 1;
            break;
        }
        gchild->delta = 0;
        n = gchild;
    }

    static void RL(Node * &n)
    {
        Node * child = n->right;
        Node * gchild = child->left;
        n->right = gchild->left;
        child->left = gchild->right;
        gchild->right = child;
        gchild->left = n;
        switch (gchild->delta) {
        case -1:
            n->delta = 0;
            child->delta = 1;
            break;
        case 0:
            n->delta = 0;
            child->delta = 0;
            break;
        case 1:
            n->delta = -1;
            child->delta = 0;
            break;
        }
        gchild->delta = 0;
        n = gchild;
    }

    static void RR(Node * &n)
    {
        Node *child = n->right;
        n->right = child->left;
        child->left = n;
        n->delta = child->delta = 0;
        n = child;
    }

    static bool recursiveAdd(Node * &n, K key, V data)
    {
        if (n == NULL) { // Caso base (folha alcançada)
            n = new Node(key, data);
            return true;
        } else if (key < n->key) {
            // Procurar posição correta à esquerda
            if (recursiveAdd(n->left, key, data)) {
                // Foi encontrada, atualizar balanço
                switch (n->delta) {
                case -1:
                    // Balanço -2: Rebalancear
                    if (n->left->delta == -1) LL(n);
                    else LR(n);
                    return false;
                case 0:
                    n->delta = -1;
                    return true;
                case 1:
                    n->delta = 0;
                    return false;
                default:
                    break;
                }
            }
        } else if (key > n->key) {
            // Procurar posição correta à direita
            if (recursiveAdd(n->right, key, data)) {
                // Foi encontrada, atualizar balanço
                switch (n->delta) {
                case -1:
                    n->delta = 0;
                    return false;
                case 0:
                    n->delta = 1;
                    return true;
                case 1:
                    // Balanço 2: Rebalancear
                    if (n->right->delta == 1) RR(n);
                    else RL(n);
                    return false;
                default:
                    break;
                }
            }
        }
        return false;
    }

    static bool recursiveRem(Node * &n, K key)
    {
        if (n == NULL);
        else if (key == n->key) { // Nó encontrado
            if (n->left == NULL && n->right == NULL) {
                delete n;
                n = NULL;
            } else if (n->left == NULL || n->right == NULL) {
                Node * aux = n;
                n = n->left != NULL ? n->left : n->right;
                aux->left = aux->right = NULL;
                delete aux;
            } else {
                Node * aux = n->left;
                while (aux->right != NULL) aux = aux->right;
                n->key = aux->key;
                n->data = aux->data;
                recursiveRem(n->left, n->key);
                switch (n->delta) {
                case -1:
                    n->delta = 0;
                    return false;
                case 0:
                    n->delta = 1;
                    return true;
                case 1:
                    if (n->right->delta == 1) RR(n);
                    else RL(n);
                    return false;
                default:
                    break;
                }
            }
            return true;
        } else if (key < n->key) {
            // Procurar posição correta à esquerda
            if (recursiveRem(n->left, key)) {
                // Foi encontrada, atualizar balanço
                switch (n->delta) {
                case -1:
                    // Balanço -2: Rebalancear
                    n->delta = 0;
                    return false;
                case 0:
                    n->delta = 1;
                    return true;
                case 1:
                    if (n->right->delta == 1) RR(n);
                    else RL(n);
                    return false;
                default:
                    break;
                }
            }
        } else {
            // Procurar posição correta à direita
            if (recursiveRem(n->right, key)) {
                // Foi encontrada, atualizar balanço
                switch (n->delta) {
                case -1:
                    if (n->left->delta == -1) LL(n);
                    else LR(n);
                    return false;
                    break;
                case 0:
                    n->delta = -1;
                    return true;
                    break;
                case 1:
                    // Balanço 2: Rebalancear
                    n->delta = 0;
                    return false;
                    break;
                default:
                    break;
                }
            }
        }
        return false;
    }

    bool recursiveSearch(Node * n, K key)
    {
        if (n == NULL) return false;
        if (key == n->key) return true;
        if (key < n->key) return recursiveSearch(n->left, key);
        return recursiveSearch(n->right, key);
    }

    Node * find(Node * n, K key)
    {
        if (n == NULL || key == n->key) return n;
        if (key < n->key) return find(n->left, key);
        return find(n->right, key);
    }

    V &getRef(K key) { return find(root, key)->data; }

    static void recursiveSerialize(QDataStream &stream, Node * n)
    {
        if (n == NULL) {
            stream << false;
            return;
        }
        stream << (bool)true << n->key << n->data << n->delta ;
        recursiveSerialize(stream, n->left);
        recursiveSerialize(stream, n->right);
    }

    static void recursiveDeserialize(QDataStream &stream, Node * &n)
    {
        bool flag;
        stream >> flag;
        if (!flag) {
            n = NULL;
            return;
        }
        K key;
        stream >> key;
        V data;
        stream >> data;
        n = new Node(key, data);
        stream >> n->delta;
        recursiveDeserialize(stream, n->left);
        recursiveDeserialize(stream, n->right);
    }

public:
    AVL() : root(NULL) {}
    ~AVL() { delete root; }

    void add(K key, V data) { recursiveAdd(root, key, data); }
    void remove(K key) { recursiveRem(root, key); }
    bool test(K key) { return recursiveSearch(root, key); }
    V get(K key)
    {
        Node * node = find(root, key);
        if (node == NULL) return V();
        return node->data;
    }

    virtual void serialize(QDataStream &stream) const
    {
        recursiveSerialize(stream, root);
    }

    virtual void deserialize(QDataStream &stream)
    {
        recursiveDeserialize(stream, root);
    }
};

#endif // AVL_H
