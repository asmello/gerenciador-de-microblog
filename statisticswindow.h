#ifndef STATISTICSWINDOW_H
#define STATISTICSWINDOW_H

#include "usermessages.h"
#include <QDialog>

namespace Ui {
class StatisticsWindow;
}

class StatisticsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit StatisticsWindow(UserMessages * usermsgs, QWidget * parent = 0);
    ~StatisticsWindow();

private:
    Ui::StatisticsWindow *ui;
};

#endif // STATISTICSWINDOW_H
