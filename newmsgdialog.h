#ifndef NEWMSGDIALOG_H
#define NEWMSGDIALOG_H

#include <QDialog>
#include <QString>

namespace Ui {
class NewMsgDialog;
}

class NewMsgDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewMsgDialog(QWidget * parent = 0);
    ~NewMsgDialog();

    QString getUser() const;
    QString getMsg() const;

private:
    Ui::NewMsgDialog * ui;
};

#endif // NEWMSGDIALOG_H
