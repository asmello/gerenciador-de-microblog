#include "newmsgdialog.h"
#include "ui_newmsgdialog.h"

NewMsgDialog::NewMsgDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewMsgDialog)
{
    ui->setupUi(this);
}

NewMsgDialog::~NewMsgDialog()
{
    delete ui;
}

QString NewMsgDialog::getMsg() const
{
    return ui->plainTextEdit->toPlainText();
}

QString NewMsgDialog::getUser() const
{
    return ui->lineEdit->text();
}
