#ifndef MESSAGES_H
#define MESSAGES_H

#include "avl.h"
#include <QList>
#include <QString>

class Messages : public AVL< QString, QList<QString> >
{
public:
    struct msglistdata {
        QString username;
        QList<QString> messages;
    };
    void add(QString user, QString msg);
    bool remove(QString user, int index);
    bool clear(QString user);
    QList<msglistdata> getSortedUserMsgs();
    QList<QString> getMessages(QString user) { return get(user); }
    int getNumMsgs(QString user);
    bool isActive(QString user) { return test(user); }
private:
    static void alphabeticalSort(Node * n, QList<msglistdata> &list);
};

#endif // MESSAGES_H
