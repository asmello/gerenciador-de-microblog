#include "messageexpandwindow.h"
#include "ui_messageexpandwindow.h"

MessageExpandWindow::MessageExpandWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MessageExpandWindow)
{
    ui->setupUi(this);
}

MessageExpandWindow::~MessageExpandWindow()
{
    delete ui;
}

void MessageExpandWindow::setMessage(QString message)
{
    ui->plainTextEdit->setPlainText(message);
}

void MessageExpandWindow::setUserInfo(QString user, int id)
{
    ui->label->setText(user);
    ui->label_2->setText(trUtf8("#%1").arg(id));
}
