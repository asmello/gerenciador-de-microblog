#include "messages.h"

void Messages::add(QString user, QString msg)
{
    if (isActive(user)) {
        QList<QString> &msgs = getRef(user);
        msgs.append(msg);
    } else {
        QList<QString> msgs;
        msgs.append(msg);
        AVL< QString, QList<QString> >::add(user, msgs);
    }
}

bool Messages::remove(QString user, int index)
{
    if (!isActive(user)) return false;
    QList<QString> &msgs = getRef(user);
    msgs.removeAt(index);
    if (msgs.isEmpty()) AVL< QString, QList<QString> >::remove(user);
    return true;
}

bool Messages::clear(QString user)
{
    if (!isActive(user)) return false;
    AVL< QString, QList<QString> >::remove(user);
    return true;
}

int Messages::getNumMsgs(QString user)
{
    if (!isActive(user)) return 0;
    QList<QString> msgs = get(user);
    return msgs.size();
}

void Messages::alphabeticalSort(Node *n, QList<Messages::msglistdata> &list)
{
    if (n == NULL) return;
    alphabeticalSort(n->left, list);
    Messages::msglistdata userdata = {n->key, n->data};
    list.append(userdata);
    alphabeticalSort(n->right, list);
}

QList<Messages::msglistdata> Messages::getSortedUserMsgs()
{
    QList<Messages::msglistdata> list;
    alphabeticalSort(root, list);
    return list;
}
