#include "usermessages.h"
#include "messageexpandwindow.h"
#include <QStringList>

UserMessages::UserMessages(UserList * userlist, QObject * parent) :
    QAbstractTableModel(parent)
{
    userstree = userlist->usertree;
    msgstree = new Messages();
    connect(this, SIGNAL(needUpdate()), this, SLOT(update()));
}

UserMessages::~UserMessages()
{
    delete msgstree;
}

int UserMessages::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    return usermsgs.size();
}

int UserMessages::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    int max = 0;
    foreach(Messages::msglistdata data, usermsgs) {
        if (data.messages.size() > max) max = data.messages.size();
    }
    return max+1;
}

QVariant UserMessages::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole) return QVariant();
    Messages::msglistdata data = usermsgs.at(index.row());
    if (index.column() == 0) return QVariant(data.username);
    if (index.column()-1 >= data.messages.size()) return QVariant("");
    return QVariant(data.messages.at(index.column()-1));
}

QVariant UserMessages::headerData(int section,
                                  Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) return QVariant();
    if (orientation == Qt::Horizontal) {
        if (section == 0) return QVariant(trUtf8("Usuário"));
        else return QVariant(section);
    } else return QVariant(section+1);
}

void UserMessages::update()
{
    emit beginResetModel();
    usermsgs = msgstree->getSortedUserMsgs();
    emit endResetModel();
}

bool UserMessages::add(QString user, QString msg)
{
    if (!userstree->isRegistered(user)) return false;
    msgstree->add(user, msg);
    emit needUpdate();
    return true;
}

bool UserMessages::remove(QString user, int index)
{
    if (!userstree->isRegistered(user)) return false;
    if (!msgstree->remove(user, index)) return false;
    emit needUpdate();
    return true;
}

bool UserMessages::clear(QString user)
{
    if (!userstree->isRegistered(user)) return false;
    if (!msgstree->clear(user)) return false;
    emit needUpdate();
    return true;
}

void UserMessages::filter(QString text)
{
    emit beginResetModel();
    for (int i = 0; i < usermsgs.size(); ++i) {
        Messages::msglistdata data = usermsgs.at(i);
        for (int j = 0; j < data.messages.size(); ++j) {
            if (!data.messages.at(j).contains(text))
                data.messages.removeAt(j--);
        }
        if (data.messages.isEmpty()) usermsgs.removeAt(i);
        else usermsgs.replace(i, data);
    }
    emit endResetModel();
}

void UserMessages::filterOut(QString text)
{
    emit beginResetModel();
    for (int i = 0; i < usermsgs.size(); ++i) {
        Messages::msglistdata data = usermsgs.at(i);
        for (int j = 0; j < data.messages.size(); ++j) {
            if (data.messages.at(j).contains(text))
                data.messages.removeAt(j--);
        }
        if (data.messages.isEmpty()) usermsgs.removeAt(i);
        else usermsgs.replace(i, data);
    }
    emit endResetModel();
}

void UserMessages::filterUsers(QString text)
{
    emit beginResetModel();
    for (int i = 0; i < usermsgs.size(); ++i) {
        if (!usermsgs.at(i).username.contains(text)) usermsgs.removeAt(i--);
    }
    emit endResetModel();
}

void UserMessages::filterOutUsers(QString text)
{
    emit beginResetModel();
    for (int i = 0; i < usermsgs.size(); ++i) {
        if (usermsgs.at(i).username.contains(text)) usermsgs.removeAt(i--);
    }
    emit endResetModel();
}

void UserMessages::writeToFile(QFile &file)
{
    QDataStream out(&file);
    out << *msgstree;
}

void UserMessages::readFromFile(QFile &file)
{
    QDataStream in(&file);
    in >> *msgstree;
    emit needUpdate();
}

int UserMessages::getNumActive()
{
    return usermsgs.size();
}

int UserMessages::getTotalMsgs()
{
    int total = 0;
    foreach(Messages::msglistdata data, usermsgs) {
        total += data.messages.size();
    }
    return total;
}

QList<QString> UserMessages::getTopKeys(int amount)
{
    QHash<QString, quint64> counter;
    foreach(Messages::msglistdata data, usermsgs) {
        foreach(QString msg, data.messages) {
            foreach(QString token, msg.split(QRegExp("\\W+"),
                                             QString::SkipEmptyParts)) {
                if (counter.contains(token)) {
                    quint64 count = counter.take(token);
                    counter.insert(token, count+1);
                } else counter.insert(token, 1);
            }
        }
    }
    if (counter.isEmpty()) return QList<QString>();
    QMultiMap<quint64, QString> tokens;
    foreach(QString token, counter.keys()) {
        tokens.insert(counter.value(token), token);
    }
    QList<QString> list;
    list = tokens.values().mid(amount > counter.size() ?
                                   0 : counter.size()-amount, amount);
    return reversed<QString>(list);
}

QList<QString> UserMessages::getTopUser()
{
    QHash<QString, quint64> counter;
    QList<QString> topkeys = getTopKeys(3);
    if (topkeys.isEmpty()) return QList<QString>();
    foreach(Messages::msglistdata data, usermsgs) {
        foreach(QString msg, data.messages) {
            foreach(QString key, topkeys) {
                if (msg.contains(key)) {
                    if (counter.contains(data.username)) {
                        quint64 count = counter.take(data.username);
                        counter.insert(data.username, count+msg.count(key));
                    } else counter.insert(data.username, msg.count(key));
                }
            }
        }
    }
    if (counter.isEmpty()) return QList<QString>();
    QMultiMap<quint64, QString> users;
    foreach(QString user, counter.keys()) {
        users.insert(counter.value(user), user);
    }
    quint64 lastkey = users.keys().last();
    QList<QString> topusers = users.values(lastkey);
    return reversed<QString>(topusers);
}

void UserMessages::cellAction(QModelIndex index)
{
    if (index.column() > 0) {
        Messages::msglistdata data = usermsgs.at(index.row());
        if (data.messages.size() <= index.column()-1) return;
        MessageExpandWindow msgdetails;
        msgdetails.setMessage(data.messages.at(index.column()-1));
        msgdetails.setUserInfo(data.username, index.column());
        msgdetails.exec();
    }
}
